package com.shanemyrick.demos.pokeapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
class PokeApiApplication

fun main(args: Array<String>) {
    runApplication<PokeApiApplication>(*args)
}

@RestController
class HomeController {
    @GetMapping("/")
    fun home() = "Hello"
}
